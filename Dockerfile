FROM python:3.9-alpine

WORKDIR /code

COPY requirements.txt ./

RUN pip install -r requirements.txt

COPY src ./

EXPOSE 20777/udp

CMD ["python3", "main.py"]