import logging
import socket
import time

import requests
from f1_2020_telemetry.packets import unpack_udp_packet, PacketID
from dotenv import load_dotenv

load_dotenv()

from config import Config

logging.basicConfig(level=logging.INFO)


class LapData:

    def __init__(self):
        self.time = None
        self.time_sector_1 = None
        self.time_sector_2 = None
        self.time_sector_3 = None
        self.tyre_id = None
        self.tyre_age = 0
        self.drs_used = False
        self.overtake_button_used = False
        self.invalid = False


class Player:

    def __init__(self):
        self.current_lap = 1
        self.driver_id = None
        self.team_id = None
        self.lap_data = LapData()


class Session:

    def __init__(self):
        self.track_id = None
        self.weather_id = None
        self.formula_id = None
        self.session_type_id = None


def run():
    udp_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    udp_socket.bind(("", Config.PORT))
    logging.info('Started UDP server at port {}'.format(Config.PORT))

    session = None
    player_1 = None
    player_2 = None
    while True:
        udp_packet = udp_socket.recv(2048)
        packet = unpack_udp_packet(udp_packet)

        if (packet.header.packetId == PacketID.EVENT and packet.eventStringCode == b'SSTA') or session is None:
            # Setup if the session is started
            logging.info('New session started')
            session = Session()
            player_1 = Player()
            player_2 = Player() if packet.header.secondaryPlayerCarIndex != 255 else None
        elif packet.header.packetId == PacketID.SESSION:
            session.weather_id = packet.weather
            session.session_type_id = packet.sessionType
            session.track_id = packet.trackId
            session.formula_id = packet.formula
        elif packet.header.packetId == PacketID.LAP_DATA:
            lap_data_p1 = packet.lapData[packet.header.playerCarIndex]
            if lap_data_p1.currentLapNum > player_1.current_lap:
                player_1.lap_data.time = lap_data_p1.lastLapTime
                player_1.lap_data.time_sector_3 = round(
                    lap_data_p1.lastLapTime - player_1.lap_data.time_sector_1 - player_1.lap_data.time_sector_2, 3)
                send_lap_data(session, player_1, 1)
                player_1.lap_data = LapData()
                player_1.current_lap += 1

            player_1.lap_data.time_sector_1 = lap_data_p1.sector1TimeInMS / 1000
            player_1.lap_data.time_sector_2 = lap_data_p1.sector2TimeInMS / 1000
            player_1.lap_data.invalid = lap_data_p1.currentLapInvalid
            # TODO: check if penalties are for this lap only and should thus be added to this lap

            if player_2 is not None:
                lap_data_p2 = packet.lapData[packet.header.secondaryPlayerCarIndex]
                if lap_data_p2.currentLapNum > player_2.current_lap:
                    player_2.lap_data.time = lap_data_p2.lastLapTime
                    player_2.lap_data.time_sector_3 = round(
                        lap_data_p2.lastLapTime - player_2.lap_data.time_sector_1 - player_2.lap_data.time_sector_2, 3)
                    send_lap_data(session, player_2, 2)
                    player_2.lap_data = LapData()
                    player_2.current_lap += 1

                player_2.lap_data.time_sector_1 = lap_data_p2.sector1TimeInMS / 1000
                player_2.lap_data.time_sector_2 = lap_data_p2.sector2TimeInMS / 1000
                player_2.lap_data.invalid = lap_data_p2.currentLapInvalid
        elif packet.header.packetId == PacketID.PARTICIPANTS:
            participant_p1 = packet.participants[packet.header.playerCarIndex]
            player_1.driver_id = participant_p1.driverId
            player_1.team_id = participant_p1.teamId
            if player_2 is not None:
                participant_p2 = packet.participants[packet.header.secondaryPlayerCarIndex]
                player_2.driver_id = participant_p2.driverId
                player_2.team_id = participant_p2.teamId
        elif packet.header.packetId == PacketID.CAR_TELEMETRY:
            telemetry_p1 = packet.carTelemetryData[packet.header.playerCarIndex]
            player_1.lap_data.drs_used = player_1.lap_data.drs_used or telemetry_p1.drs == 1

            if player_2 is not None:
                telemetry_p2 = packet.carTelemetryData[packet.header.secondaryPlayerCarIndex]
                player_2.lap_data.drs_used = player_2.lap_data.drs_used or telemetry_p2.drs == 1
        elif packet.header.packetId == PacketID.CAR_STATUS:
            status_p1 = packet.carStatusData[packet.header.playerCarIndex]
            player_1.lap_data.tyre_id = status_p1.actualTyreCompound
            player_1.lap_data.tyre_age = status_p1.tyresAgeLaps
            player_1.lap_data.overtake_button_used = player_1.lap_data.overtake_button_used or \
                                                 (status_p1.ersDeployMode == 2 and packet.header.sessionTime > 0.1)

            if player_2 is not None:
                status_p2 = packet.carStatusData[packet.header.secondaryPlayerCarIndex]
                player_2.lap_data.tyre_id = status_p2.actualTyreCompound
                player_2.lap_data.tyre_age = status_p2.tyresAgeLaps
                player_2.lap_data.overtake_button_used = player_2.lap_data.overtake_button_used or \
                                                 (status_p2.ersDeployMode == 2 and packet.header.sessionTime > 0.1)


def send_lap_data(session: Session, player: Player, player_no: int):
    if player.lap_data.invalid:
        logging.info('Invalid lap, so not sending data')
        return

    logging.info('Sending data to API')
    body = {
        'player_no': player_no,
        'lap_no': player.current_lap,
        'time': str(player.lap_data.time),
        'time_sector_1': player.lap_data.time_sector_1,
        'time_sector_2': player.lap_data.time_sector_2,
        'time_sector_3': player.lap_data.time_sector_3,
        'timestamp': int(time.time()),
        'driver_id': player.driver_id,
        'team_id': player.team_id,
        'weather_id': session.weather_id,
        'formula_id': session.formula_id,
        'session_type_id': session.session_type_id,
        'tyre_id': player.lap_data.tyre_id,
        'tyre_age': player.lap_data.tyre_age,
        'drs_used': player.lap_data.drs_used,
        'overtake_button_used': player.lap_data.overtake_button_used
    }
    logging.info('{}'.format(body))

    r = requests.post(
        '{}/tracks/{}/laps'.format(Config.API_URL, session.track_id),
        headers={
            'X-API-Token': Config.API_TOKEN
        },
        json=body
    )

    if r.status_code != 201:
        logging.error('Could not post lap data: {}'.format(r.json()))


if __name__ == '__main__':
    run()
