import os


class Config:
    PORT = int(os.environ.get('PORT', 20777))
    API_URL = os.environ.get('API_URL')
    API_TOKEN = os.environ.get('API_TOKEN')
